@extends('adminlte::page')

@section('title', 'IEGM@ADMIN - Configurações')

@section('content_header')
    <h1>Configurações</h1>
@stop

@section('content_title')
    <h3 class="box-title">Configurações</h3>
    <a href="{{ url()->previous() }}" class="btn btn-xs btn-default pull-right"><i class="fa fa-arrow-circle-left"></i> VOLTAR</a>
@stop

@section('content')

  
  
  <div class="row">
  	<div class="col-md-12">
  		<div class="row">
        <div class="col-md-8 col-md-offset-2">
          @if (\Session::has('success'))
            <div class="alert alert-success">
              <ul>
                  <li>{!! \Session::get('success') !!}</li>
              </ul>
            </div>
          @endif
        </div>
      </div>
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          @include('partials.alert')
        </div>
      </div>

      {!! Form::model($configuration, ['route' => 'configuration.store', 'class' => 'form-horizontal', 'files' => true]) !!}

        @include('configuration.form')
      
      {!! Form::close() !!}

  	</div>
  </div>

@stop