<div class="form-group">
  {!! Form::label('logo', 'Logo', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-file-image-o"></i></div>
    {!! Form::file('logo', null, ['class' => 'form-control']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('slide1', 'Primeira Imagem', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-file-image-o"></i></div>
    {!! Form::file('slide1', null, ['class' => 'form-control']); !!}
  </div>
</div>
<div class="form-group">
  {!! Form::label('text1', 'Texto do Primeiro Slide', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-newspaper-o"></i></div>
    {!! Form::textarea('text1', $configuration->text1, ['class' => 'form-control', 'id' => 'artigo-ckeditor1']); !!}
  </div>
</div>
<div class="form-group">
  {!! Form::label('link1', 'Link do Slide', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-bookmark"></i></div>
    {!! Form::text('link1', $configuration->link1, ['class' => 'form-control']); !!}
  </div>
</div>
<hr>
<div class="form-group">
  {!! Form::label('slide2', 'Segunda Imagem', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-file-image-o"></i></div>
    {!! Form::file('slide2', null, ['class' => 'form-control']); !!}
  </div>
</div>
<div class="form-group">
  {!! Form::label('text2', 'Texto do Segundo Slide', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-newspaper-o"></i></div>
    {!! Form::textarea('text2', $configuration->text2, ['class' => 'form-control', 'id' => 'artigo-ckeditor2']); !!}
  </div>
</div>
<div class="form-group">
  {!! Form::label('link2', 'Link do Slide', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-bookmark"></i></div>
    {!! Form::text('link2', $configuration->link2, ['class' => 'form-control']); !!}
  </div>
</div>
<hr>
<div class="form-group">
  {!! Form::label('slide3', 'Terceira Imagem', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-file-image-o"></i></div>
    {!! Form::file('slide3', null, ['class' => 'form-control']); !!}
  </div>
</div>
<div class="form-group">
  {!! Form::label('text3', 'Texto do Terceiro Slide', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-newspaper-o"></i></div>
    {!! Form::textarea('text3', $configuration->text3, ['class' => 'form-control', 'id' => 'artigo-ckeditor3']); !!}
  </div>
</div>
<div class="form-group">
  {!! Form::label('link3', 'Link do Slide', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-bookmark"></i></div>
    {!! Form::text('link3', $configuration->link3, ['class' => 'form-control']); !!}
  </div>
</div>
<hr>
<div class="form-group">
  {!! Form::label('teachers', 'Link Docentes', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-user"></i></div>
    {!! Form::text('teachers', $configuration->teachers, ['class' => 'form-control']); !!}
  </div>
</div>
<hr>
<div class="form-group">
  {!! Form::label('students', 'Link Estudantes', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-user"></i></div>
    {!! Form::text('students', $configuration->students, ['class' => 'form-control']); !!}
  </div>
</div>
<hr>
<div class="form-group">
  {!! Form::label('facebook', 'Facebook', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-facebook-official"></i></div>
    {!! Form::text('facebook', $configuration->facebook, ['class' => 'form-control']); !!}
  </div>
</div>
<hr>
<div class="form-group">
  {!! Form::label('instagram', 'Instagram', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-instagram"></i></div>
    {!! Form::text('instagram', $configuration->instagram, ['class' => 'form-control']); !!}
  </div>
</div>
<hr>
<div class="form-group">
  {!! Form::label('email', 'E-mail', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
    {!! Form::text('email', $configuration->email, ['class' => 'form-control']); !!}
  </div>
</div>
<hr>
<div class="form-group">
  {!! Form::label('phone', 'Telefone', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-phone"></i></div>
    {!! Form::text('phone', $configuration->phone, ['class' => 'form-control']); !!}
  </div>
</div>

<div class="text-center">
	{!! Form::submit('Gravar', ['class' => 'btn btn-success'])!!}
</div>
