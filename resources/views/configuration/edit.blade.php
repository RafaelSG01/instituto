@extends('adminlte::page')

@section('title', 'IEGM@ADMIN - Cursos')

@section('content_header')
    <h1>Categorias</h1>
@stop

@section('content_title')
    <h3 class="box-title">Editar Categoria</h3>
    <a href="{{ url()->previous() }}" class="btn btn-xs btn-default pull-right"><i class="fa fa-arrow-circle-left"></i> VOLTAR</a>
@stop

@section('content')
  
  <div class="row">
  	<div class="col-md-12 col-md-offset-0 col-xs-10 col-xs-offset-1">
  		{!! Form::model($configuration, ['route' => 'configuration.store', 'class' => 'form-horizontal']) !!}

        @include('configuration.form')
      
      {!! Form::close() !!}

  	</div>
  </div>

@stop