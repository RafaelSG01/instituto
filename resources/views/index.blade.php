@extends('layouts.app')

@section('content')
	<div class="slideshow">
	  <div class="iegm-slide current" style="background-image:url(' {{ asset('images/'.$configuration->slide1) }}' );">
	  	<div class="shadow">
		  	<div class="container">
		  		<div class="row">
		  			<div class="col-md-11 col-md-offset-1">
		  				<div class="slide-box-1" id="text">
				  			<?php echo  "$configuration->text1" ;?>
				  			<button type="button" class="btn btn-primary"><a href="{{ $configuration->link1 }}">Saiba Mais</a></button>
			  			</div>
		  			</div>
		  		</div>
		  	</div>
	  	</div>
	  </div>
	  <div class="iegm-slide" style="background-image:url( '{{ asset('images/'.$configuration->slide2) }}' );">
			<div class="shadow">
		  	<div class="container">
		  		<div class="row">
		  			<div class="col-md-10 text-right">
		  				<div class="slide-box-2" >
				  		<?php echo  "$configuration->text2" ;?>
				  			<button type="button" class="btn btn-primary"><a href="{{ $configuration->link2 }}">Saiba Mais</a></button>
			  			</div>
		  			</div>
		  		</div>
		  	</div>
			</div>
	  </div>
	  <div class="iegm-slide" style="background-image:url( '{{ asset('images/'.$configuration->slide3) }}' );">
			<div class="shadow">
		  	<div class="container">
		  		<div class="row">
		  			<div class="col-md-12 text-center">
		  				<div class="slide-box-3">
				  			<?php echo  "$configuration->text3" ;?>
				  			<button type="button" class="btn btn-primary"><a href="{{ $configuration->link3 }}">Saiba Mais</a></button>
			  			</div>
		  			</div>
		  		</div>
		  	</div>
		  </div>
	  </div>
	</div>

@endsection