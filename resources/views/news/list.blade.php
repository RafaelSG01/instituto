@extends('adminlte::page')

@section('title', 'IEGM@ADMIN - Notícias')

@section('content_header')
    <h1>Notícias</h1>
@stop

@section('content_title')
    <h3 class="box-title">Lista</h3> 
    <a href="{{ route('news.create') }}" class="btn btn-xs btn-success pull-right"><i class="fa fa-plus"></i> CRIAR</a>
@stop

@section('content')
  
  <div class="row">
  	<div class="col-md-8 col-md-offset-2">
  		<table class="table table-bordered">
  			<thead>
  				<tr>
  					<th>Nótícias</th>
  					<th>Gerenciar</th>
  				</tr>
  			</thead>
  			<tbody>
  				@forelse($posts as $post)
          <tr>
            <td>{{ $post->title }}</td>
            <td style="width: 1%; white-space: nowrap;">
              <a href="{{ route('news.destroy', $post->id) }}" class="btn btn-xs btn-danger" onclick="return confirm('Tem certeza que deseja excluir o curso?')"><i class="fa fa-trash"></i> <span class="hidden-xs">Excluir</span></a>
              <a href="{{ route('news.edit',  $post->slug) }}" class="btn btn-xs btn-info"><i class="fa fa-pencil"></i> <span class="hidden-xs">Editar</span></a>
            </td>
          </tr>
          @empty
          <tr>
            <td colspan="100" class="text-center">
              Não há cursos cadastrados.
            </td>
          </tr>
          @endforelse
  			</tbody>
  		</table>
  	</div>
  </div>

@stop