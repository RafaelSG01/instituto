@extends('adminlte::page')

@section('title', 'IEGM@ADMIN - Notícias')

@section('content_header')
    <h1>Notícias</h1>
@stop

@section('content_title')
    <h3 class="box-title">Novo Notícia</h3>
    <a href="{{ url()->previous() }}" class="btn btn-xs btn-default pull-right"><i class="fa fa-arrow-circle-left"></i> VOLTAR</a>
@stop

@section('content')
  
  <div class="row">
  	<div class="col-md-12">
  		
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          @include('partials.alert')
        </div>
      </div>

      {!! Form::open(['route' => 'news.store', 'class' => 'form-horizontal', 'files' => true]) !!}


        @include('news.form')
      
      {!! Form::close() !!}

  	</div>
  </div>

@stop