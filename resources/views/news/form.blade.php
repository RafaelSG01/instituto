<div class="form-group">
  {!! Form::label('photo', 'Imagem', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-bookmark"></i></div>
    {!! Form::file('photo', null, ['class' => 'form-control']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('title', 'Título', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa  fa-paint-brush"></i></div>
    {!! Form::text('title', null, ['class' => 'form-control']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('text', 'Notícia', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-newspaper-o"></i></div>
    {!! Form::textarea('text', null, ['class' => 'form-control', 'id' => 'artigo-ckeditor1']); !!}
  </div>
</div>

<div class="text-center">
	{!! Form::submit('Publicar', ['class' => 'btn btn-success'])!!}
</div>