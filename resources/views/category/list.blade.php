@extends('adminlte::page')

@section('title', 'IEGM@ADMIN - Cursos')

@section('content_header')
    <h1>Categorias</h1>
@stop

@section('content_title')
    <h3 class="box-title">Lista</h3>
    <a href="{{ route('category.create') }}" class="btn btn-xs btn-success pull-right"><i class="fa fa-plus"></i> CRIAR</a>
@stop

@section('content')
  
  <div class="row">
  	<div class="col-md-8 col-md-offset-2">
  		<table class="table table-bordered">
  			<thead>
  				<tr>
  					<th>Categoria</th>
  					<th>Gerenciar</th>
  				</tr>
  			</thead>
  			<tbody>
  				@forelse($categories as $category)
          <tr>
            <td><a href="{{ route('course.list', ['slug' => $category->slug]) }}">{{ $category->name }}</a></td>
            <td style="width: 1%; white-space: nowrap;">
              <a href="{{ route('category.destroy', $category->id) }}" class="btn btn-xs btn-danger" onclick="return confirm('Tem certeza que deseja excluir o categoria?')"><i class="fa fa-trash"></i> <span class="hidden-xs">Excluir</span></a>
              <a href="{{ route('category.edit', $category->slug) }}" class="btn btn-xs btn-info"><i class="fa fa-pencil"></i> <span class="hidden-xs">Editar</span></a>
              <a href="{{ route('course.list', ['slug' => $category->slug]) }}" class="btn btn-xs btn-primary"><i class="fa fa-book"></i> <span class="hidden-xs">Cursos</span></a>
            </td>
          </tr>
          @empty
          <tr>
            <td colspan="100" class="text-center">
              Não há categorias cadastradas.
            </td>
          </tr>
          @endforelse
  			</tbody>
  		</table>
  	</div>
  </div>

@stop