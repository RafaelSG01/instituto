@extends('layouts.head')

@section('content')
	<div class="container">
		<br>
		<div class="row">
	    <div class="col-md-12">
        <img src="{{asset('images/'.$communication->photo_news)}}" class="img-responsive">
      </div>
    </div>
    <div class="row">
    	<div class="col-md-12">
    		<small>Publicado em {{$communication->created_at}}</small>
        <h1>{{$communication->title_news}}</h1>
      </div>
    </div>
    <div class="row">
    	<div class="col-md-12">
    		<?php echo "$communication->news_text" ?>
    	</div>
    </div>
	</div>
@endsection