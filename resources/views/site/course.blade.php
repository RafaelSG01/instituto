@extends('layouts.head')

@section('content')
		<div class="container">
			<br><br>
  		<div class="row">
  			<div class="col-md-4 col-md-offset-2">  				  				
					<div class="box-iegm">
			    	<h4>{{$category->name}}</h4>
			    </div>
			  </div>
		    <div class="col-sm-4 select-course">
		    	{!! Form::select('courses', $category->courses->pluck('name', 'id'), null, ['class' => 'form-control', 'placeholder' => 'Escolha o curso']); !!}
		    </div>
    	</div>
    </div>
	    <div class="container">
				<div class="row">
					<div class="col-md-12">
						<h2 class="course-name">{{ $category->courses[0]->name }}</h2>
						<p class="course-description">{{ $category->courses[0]->description }}</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 col-md-offset-3 course-curriculum">
						<h2>Grade curricular</h2>
						{!! $category->courses[0]->curriculum !!}
						<button type="button" class="btn btn-primary">Matricule-se</button>
					</div>
				</div>
  		</div>
			<br><br>
		</div>
		


@endsection


@section('js')

<script>
	$(function() {

		$('select[name=courses]').change(function() {
			var course = $(this).val();

			$.get('/curso/' + course, function(curso) {
				console.log(curso);
				$('.course-name').html(curso.name);
				$('.course-curriculum').html(curso.curriculum);
				$('.course-description').html(curso.description);

			}).fail(function(){
				$('.course-name').html('');
				$('.course-curriculum').html('');
				$('.course-description').html('');
			});

		});
	})
</script>

@endsection