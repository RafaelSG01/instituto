@extends('layouts.head')

@section('content')
		<div class="container">
			<ul>
			    @if(\Session::has('inform'))
			    	<div class="alert alert-danger" role="alert">
			    		<br>
			        {{\Session::get('inform')}}
			      </div>
			    @endif
			</ul>
			@if(\Session::has('message'))
			    <div class="alert alert-info">
			    	<br>
			      {{\Session::get('message')}}
			    </div>
			@endif
			<div class="row top-margin">
				<div class="col-md-6">
					{!! Form::open(['route' => 'contact', 'files' => true, 'method' => 'post', 'action' => 'ContactController@store']) !!}

						<div class="form-group">
						    {!! Form::label('name','Seu Nome', array('class' => 'control-label')) !!}
						    {!! Form::text('name', null, array('required', 'class'=>'form-control')) !!}
						</div>

						<div class="form-group">
						    {!! Form::label('email','Seu E-mail', array('class' => 'control-label')) !!}
						    {!! Form::text('email', null, array('required', 'class'=>'form-control')) !!}
						</div>

						<div class="form-group">
						    {!! Form::label('message','Sua Mensagem', array('class' => 'control-label')) !!}
						    {!! Form::textarea('message', null, array('required', 'class'=>'form-control', 'rows' => '10')) !!}
						</div>
						<div class="g-recaptcha" data-sitekey="6LcHmSkUAAAAACSsCFB5uBl9qaGwNdETEPxQ4Us4" data-callback="nSubmit"></div>

						<div class="form-group">
						    {!! Form::submit('Enviar', ['class' => 'btn btn-primary g-recaptcha', ]); !!}
						</div>
					{!! Form::close() !!}
				</div>
				<div class="row">
					<div class="col-md-6">
						<center><h5>INSTITUTO DE EDUCAÇÃO GOMES DE MELO - IEGM</h5></center>
						<!-- Nav tabs -->
					  <ul class="nav nav-tabs" role="tablist">
					    <li role="presentation" class="active"><a href="#profissional" aria-controls="profissional" role="tab" data-toggle="tab">Secretaria Regional de Baturité:<br>Praça Valdemir Falcão, Nº1876,<br>Centro, Baturité/CE <br>CEP:62.760-000</a></li>
					    <li role="presentation"><a href="#pos" aria-controls="pos" role="tab" data-toggle="tab">Secretaria Regional de Fortaleza:<br>Av. Ministro José Américo, Nº500, SL04 <br> Cambeba, Fortaleza/CE <br> CEP : 60830-070</a></li>   
					  </ul>
					  <center><address>
						 	<ul class="list-inline">
								<li><strong><abbr title="Phone">Tel: </abbr></strong>(85) 3013-5411</li>
								<li><strong>E-mail: </strong>contato@institutogomesdemelo.com.br</li>
							</ul>
						</address></center>

					  <!-- Tab panes -->
					  <div class="tab-content">
					    <div role="tabpanel" class="tab-pane active" id="profissional">
						    <div class="row">
						    	<iframe class="iegm-map shadow-default" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d909.2307028534277!2d-38.88149728049634!3d-4.3299973919725545!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7bf3b36665f7cb5%3A0x4fe3c578de9b70ca!2sPra%C3%A7a+Valdemar+Falc%C3%A3o+-+Centro%2C+Baturit%C3%A9+-+CE%2C+62760-000!5e1!3m2!1spt-BR!2sbr!4v1489412992936" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					    	</div>
					    </div>
					    <div role="tabpanel" class="tab-pane" id="pos">
					    	<div class="row">
					    		<div >
						    		<iframe class="iegm-map shadow-default" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1980.9965921727319!2d-38.49643684480003!3d-3.8111679989876723!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7c74f8fd49e610f%3A0xfc1bd130ae31532c!2sAv.+Ministro+Jos%C3%A9+Am%C3%A9rico%2C+500+-+Sala+01+-+Cambeba%2C+Fortaleza+-+CE%2C+60822-315!5e1!3m2!1spt-BR!2sbr!4v1489158263937" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
						    	</div>
					    	</div>
					    </div>					    
					  </div>
					</div>
				</div>
			</div>
			<br><br>
		</div>
@endsection