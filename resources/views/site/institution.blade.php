@extends('layouts.head')

@section('content')
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3>Missão</h3>
					<p>"Desenvolver e formar pessoas por meio do ensino, da pesquisa e de ações humanísticas para a vida profissional e o exercício consciente da cidadania."</p>
					<h3>Visão</h3>
					<p>"Ser reconhecido como um centro de excelência de formação profissional e um importante parceiro para o desenvolvimento sócio-econômico e sustentável da iniciativa privada e do poder público."</p>
					<div class="table-responsive">
						<h3>Valores</h3>
						<table class="table table-striped">
							<tr>
								<td><i class="fa fa-check-square-o" aria-hidden="true"></i> Família Como Base Primária</td>
								<td><i class="fa fa-check-square-o" aria-hidden="true"></i> Desenvolvimento Humano</td>
							</tr>
							<tr>
								<td><i class="fa fa-check-square-o" aria-hidden="true"></i> Transparência e Ética nas Relações</td>
								<td><i class="fa fa-check-square-o" aria-hidden="true"></i> Responsabilidade Social e Desenvolvimento Sustentável</td>
							</tr>
							<tr>
								<td><i class="fa fa-check-square-o" aria-hidden="true"></i> Princípios Humanistas e Igualitários</td>
								<td><i class="fa fa-check-square-o" aria-hidden="true"></i> Comprometimento com a Qualidade de Ensino.</td>
							</tr>
						</table>
					</div>
					<div class="table-responsive">
						<h3>Áreas de Atuação</h3>
						<table class="table table-striped">
							<tr>
								<td><i class="fa fa-check-square-o" aria-hidden="true"></i> Gestão e Negócio</td>
								<td><i class="fa fa-check-square-o" aria-hidden="true"></i> Formação Tecnológica</td>
							</tr>
							<tr>
								<td><i class="fa fa-check-square-o" aria-hidden="true"></i> Formação Servidores Públicos</td>
								<td><i class="fa fa-check-square-o" aria-hidden="true"></i> Gastronomia</td>
							</tr>
							<tr>
								<td><i class="fa fa-check-square-o" aria-hidden="true"></i> Cursos de Línguas Estrangeiras</td>
								<td><i class="fa fa-check-square-o" aria-hidden="true"></i> In Companny</td>
							</tr>
							<tr>
								<td><i class="fa fa-check-square-o" aria-hidden="true"></i> Curso de Formação</td>
								<td><i class="fa fa-check-square-o" aria-hidden="true"></i> Turismo</td>
							</tr>
							<tr>
								<td><i class="fa fa-check-square-o" aria-hidden="true"></i> Workshops em Diversas Áreas</td>
								<td><i class="fa fa-check-square-o" aria-hidden="true"></i> Ação Socia</td>
							</tr>
						</table>
					</div>
				</div>
			</div>	
			<br><br>
		</div>
@endsection