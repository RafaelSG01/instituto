@extends('layouts.head')

@section('content')
	<div class="container">
		<br>
		@foreach($posts as $post)
		<br>
			<div class="row">				
				<div class="col-md-2 col-md-offset-1">
					<div class="thumbnail">
						<img src="{{asset('images/'.$post->photo)}}" class="img-responsive img-circle">
					</div>
				</div>
				<div class="col-md-8">
					<small>Publicado em {{$post->created_at}}</small>
					<a href="{{ route('course', ['slug' => $post->slug]) }}"><h3>{{$post->title}}</h3></a>
				</div>
			</div>
		@endforeach
		
	</div>
@endsection