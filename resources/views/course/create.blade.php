@extends('adminlte::page')

@section('title', 'IEGM@ADMIN - Cursos')

@section('content_header')
    <h1>Curso</h1>
@stop

@section('content_title')
    <h3 class="box-title">Novo Curso</h3>
    <a href="{{ url()->previous() }}" class="btn btn-xs btn-default pull-right"><i class="fa fa-arrow-circle-left"></i> VOLTAR</a>
@stop

@section('content')
  
  <div class="row">
  	<div class="col-md-12">
  		
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          @include('partials.alert')
        </div>
      </div>

      {!! Form::open(['route' => 'course.store', 'class' => 'form-horizontal']) !!}

        {!! Form::hidden('category_id', $category->id) !!}

        @include('course.form')
      
      {!! Form::close() !!}

  	</div>
  </div>

@stop