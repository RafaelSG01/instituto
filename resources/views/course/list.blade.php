@extends('adminlte::page')

@section('title', 'IEGM@ADMIN - Cursos')

@section('content_header')
    <h1>Cursos</h1>
@stop

@section('content_title')
    <h3 class="box-title">Lista</h3>
    <a href="{{ url()->previous() }}" class="btn btn-xs btn-default pull-right"><i class="fa fa-arrow-circle-left"></i> VOLTAR</a> 
    <a href="{{ route('course.create',['slug' => $category->slug]) }}" class="btn btn-xs btn-success pull-right"><i class="fa fa-plus"></i> CRIAR</a>
@stop

@section('content')
  
  <div class="row">
  	<div class="col-md-8 col-md-offset-2">
  		<table class="table table-bordered">
  			<thead>
  				<tr>
  					<th>Curso</th>
  					<th>Gerenciar</th>
  				</tr>
  			</thead>
  			<tbody>
  				@forelse($courses as $course)
          <tr>
            <td>{{ $course->name }}</td>
            <td style="width: 1%; white-space: nowrap;">
              <a href="{{ route('course.destroy', $course->id) }}" class="btn btn-xs btn-danger" onclick="return confirm('Tem certeza que deseja excluir o curso?')"><i class="fa fa-trash"></i> <span class="hidden-xs">Excluir</span></a>
              <a href="{{ route('course.edit', ['slug'=> $course->slug]) }}" class="btn btn-xs btn-info"><i class="fa fa-pencil"></i> <span class="hidden-xs">Editar</span></a>
            </td>
          </tr>
          @empty
          <tr>
            <td colspan="100" class="text-center">
              Não há cursos cadastrados.
            </td>
          </tr>
          @endforelse
  			</tbody>
  		</table>
  	</div>
  </div>

@stop