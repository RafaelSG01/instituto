<div class="form-group">
  {!! Form::label('name', 'Curso', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-bookmark"></i></div>
    {!! Form::text('name', null, ['class' => 'form-control']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('teacher', 'Professor', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa  fa-paint-brush"></i></div>
    {!! Form::text('teacher', null, ['class' => 'form-control']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('date', 'Data de Início', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
    {!! Form::date('date', \Carbon\Carbon::now(), ['class' => 'form-control']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('description', 'Descrição', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-newspaper-o"></i></div>
    {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'artigo-ckeditor1']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('curriculum', 'Gradre Curricular', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-map-marker"></i></div>
    {!! Form::textarea('curriculum', null, ['class' => 'form-control', 'id' => 'artigo-ckeditor2']); !!}
  </div>
</div>

{!! Form::hidden('value', '0') !!}

<div class="text-center">
	{!! Form::submit('Gravar', ['class' => 'btn btn-success'])!!}
</div>