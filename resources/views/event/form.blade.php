<div class="form-group">
  {!! Form::label('slide1', 'Primeira Imagem', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-file-image-o"></i></div>
    {!! Form::file('slide1', null, ['class' => 'form-control']); !!}
  </div>
</div>
<div class="form-group">
  {!! Form::label('slide2', 'Segunda Imagem', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-file-image-o"></i></div>
    {!! Form::file('slide2', null, ['class' => 'form-control']); !!}
  </div>
</div>
<div class="form-group">
  {!! Form::label('slide3', 'Terceira Imagem', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-file-image-o"></i></div>
    {!! Form::file('slide3', null, ['class' => 'form-control']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('title', 'Evento', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-bookmark"></i></div>
    {!! Form::text('title', null, ['class' => 'form-control']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('color', 'Cor Tema', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa  fa-paint-brush"></i></div>
    {!! Form::text('color', null, ['class' => 'form-control']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('description', 'Sobre', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-newspaper-o"></i></div>
    {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'artigo-ckeditor1']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('initial_date', 'Início', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
    {!! Form::date('initial_date', \Carbon\Carbon::now(), ['class' => 'form-control']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('end_date', 'Fim', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
    {!! Form::date('end_date', \Carbon\Carbon::now(), ['class' => 'form-control']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('place_map', 'Localização no Mapa', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-map-marker"></i></div>
    {!! Form::text('place_map', null, ['class' => 'form-control']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('place', 'Endereço', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-map-marker"></i></div>
    {!! Form::textarea('place', null, ['class' => 'form-control', 'id' => 'artigo-ckeditor2']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('facebook', 'Facebook', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-facebook-official"></i></div>
    {!! Form::text('facebook', null, ['class' => 'form-control']); !!}
  </div>
</div>
<hr>
<div class="form-group">
  {!! Form::label('instagram', 'Instagram', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-instagram"></i></div>
    {!! Form::text('instagram', null, ['class' => 'form-control']); !!}
  </div>
</div>
<hr>
<div class="form-group">
  {!! Form::label('email', 'E-mail', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
    {!! Form::text('email', null, ['class' => 'form-control']); !!}
  </div>
</div>
<hr>
<div class="form-group">
  {!! Form::label('phone', 'Telefone', ['class' => 'control-label col-sm-1 col-md-offset-2']) !!}
  <div class="col-sm-6 input-group">
    <div class="input-group-addon"><i class="fa fa-phone"></i></div>
    {!! Form::text('phone', null, ['class' => 'form-control']); !!}
  </div>
</div>

<div class="text-center">
	{!! Form::submit('Gravar', ['class' => 'btn btn-success'])!!}
</div>