<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>IEGM - Instituto de Educaçã Gomes de Melo</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/camera.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <link src="{{asset('css/summernote.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12 up-iegm">
                <ul class="list-inline access-user pull-right">
                    <li><i class="fa fa-user-circle-o" aria-hidden="true"></i> Docentes</li>
                    <li><i class="fa fa-user-circle-o" aria-hidden="true"></i> Alunos</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 up-iegm">
                <hr class="hr-iegm">
            </div>
        </div>
    </div>  
    <nav class="navbar navbar-default">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a href="{{route('index')}}"><img src="{{ asset('images/'.$configuration->logo)}}" class="img-responsive"></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li class="box-menu curmudgeon"><a href="{{route('institution')}}">Instituição <span class="sr-only">(current)</span></a></li>
            <li class="box-menu curmudgeon dropdown"><a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Cursos <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    @foreach($categories as $category)
                        <li><a href="{{route('category.courses', ['slug' => $category->slug])}}">{{ $category->name }}</a></li>
                    @endforeach
                </ul> 
            </li>
            <li class="box-menu curmudgeon dropdown"><a lass="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Eventos <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    @foreach($events as $event)
                        <li><a href="{{route('event', ['slug' => $event->slug])}}">{{ $event->title }}</a></li>
                    @endforeach
                </ul>
            </li>
            <li class="box-menu curmudgeon"><a href="{{route('post')}}">Imprensa</a></li>
            <li class="box-menu curmudgeon"><a href="{{route('contact')}}">Contatos</a></li>            
          </ul>
        </div><!-- /.navbar-collapse -->        
      </div><!-- /.container-fluid -->
      <div class="container">
        <div class="row">
            <div class="col-md-12">
                <hr class="hr-iegm">
            </div>
        </div>
    </div>  
    </nav>

    @yield('content')

<!-- footer -->
    <footer>
        <div class="container up-iegm">        
    </div>
        
        <div class="container footer-iegm">
            <div class="row up-iegm">
            <div class="col-md-12">
                <hr class="hr-iegm">
            </div>
        </div>
            <div class="row text-center">
                <div class="col-md-5 up-iegm">
                    <ul class="list-inline">
                        <li><strong>Tel: </strong>{{$configuration->phone}}</li>
                        <li><strong>E-mail: </strong>{{$configuration->email}}</li>
                    </ul>
                </div>
                <div class="col-md-2 up-iegm">
                  <ul class="list-inline">
                    <li><a href="{{$configuration->facebook}}" target="_blank"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
                    <li><a href="{{$configuration->instagram}}" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                  </ul>
                </div>
                <div class="col-md-5 up-iegm">
                    <p><b>@copyright - 2014</b> - Instituto de Educação Gomes de Melo</p>                   
                </div>
            </div>
        </div>
    </footer>



    <script src="{{ asset('js/jquery.min.js')}}"></script>
    <script src="{{ asset('js/jquery.mobile.customized.min.js')}}"></script>
    <script src="{{ asset('js/jquery.easing.1.3.js')}}"></script>
    <script src="{{ asset('js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('js/scripts.js')}}"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script>
        var onLoadCallback = function () { 
         grecaptcha.render('recaptchaContainer', { 'sitekey' : '6LcHmSkUAAAAACSsCFB5uBl9qaGwNdETEPxQ4Us4', 'callback' : verifyCallback, 'theme' : 'light' }); 
        };
    </script>
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
  <script src="/vendor/unisharp/laravel-ckeditor/styles.js"></script>

</body>
</html>
