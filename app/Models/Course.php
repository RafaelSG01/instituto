<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
  protected $fillable = [
  	'category_id',
		'name',
		'teacher',
		'slug',
		'date',
		'description',
		'curriculum',
		'value'
  ];

	public function category()
	{
		return $this->belongsTo(Category::class);
	}
}
