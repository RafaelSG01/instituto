<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
  protected $fillable = [
    'slide1', 'slide2', 'slide3', 'text1', 'text2', 'text3','link1', 'link2', 'link3', 'logo', 'facebook', 'instagram', 'email', 'phone', 'students', 'teachers'];
}
