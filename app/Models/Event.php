<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
  
	protected $fillable = [
		'title',
		'slug',
		'color',
		'description',
		'place_map',
		'place',
		'initial_date',
		'end_date',
		'status',
		'slide1',
		'slide2',
		'slide3',
		'facebook', 
		'instagram', 
		'email', 
		'phone'
	];

	public function programs()
	{
		return $this->hasMany(Program::class);
	}

	public function publications()
	{
		return $this->hasMany(Publication::class);
	}

}
