<?php 

namespace App\Repositories;

use App\Repositories\Support\AbstractRepository;

use App\Models\Post;

class PostRepository extends AbstractRepository
{
	
	protected $modelClassName = Post::class;

	public function create(array $request)
	{
		$request['slug'] = str_slug($request['title'], '-');

		return Post::create($request);
	}

	public function findBySlug($slug)
	{
		return Post::where('slug', $slug)->first();
	}

	
}