<?php 

namespace App\Repositories;

use App\Repositories\Support\AbstractRepository;

use App\Models\Category;

class CategoryRepository extends AbstractRepository
{
	
	protected $modelClassName = Category::class;

	public function create(array $request)
	{
		$request['slug'] = str_slug($request['name'], '-');

		return Category::create($request);
	}

	public function findBySlug($slug)
	{
		return Category::where('slug', $slug)->first();
	}
	
}