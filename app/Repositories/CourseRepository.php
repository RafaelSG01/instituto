<?php 

namespace App\Repositories;

use App\Repositories\Support\AbstractRepository;

use App\Models\Course;

class CourseRepository extends AbstractRepository
{
	
	protected $modelClassName = Course::class;

	public function create(array $request)
	{
		$request['slug'] = str_slug($request['name'], '-');

		return Course::create($request);
	}

	public function findBySlug($slug)
	{
		return Course::where('slug', $slug)->first();
	}
	
}