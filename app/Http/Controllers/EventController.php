<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\EventRepository;
use App\Models\Event;

class EventController extends Controller
{
  
  protected $event;

  public function __construct(EventRepository $event) {
  	$this->event = $event;
  }
	
	public function list()
	{
		$events = $this->event->all();

		return view('event.list', compact('events'));
	}

  public function index(){
    return view('site.event.index');
  }


	public function create()
	{
		return view('event.create');
	}

	public function edit($slug)
	{
		$event = $this->event->findBySlug($slug);

		return view('event.edit', compact('event'));
	}

	public function store(Request $request)
	{
		$event = $this->event->create($request->toArray());


      $slide1 = $request->file('slide1');
      $slide2 = $request->file('slide2');
      $slide3 = $request->file('slide3');

      if ($request->hasfile('slide1')){
        $slide1_filename = md5(date('slide1 m/d/Y h:i:s a', time())).".".$slide1->getClientOriginalExtension();
        $slide1->move(public_path('images'), $slide1_filename);
        $event['slide1'] = $slide1_filename;
      }

      if ($request->hasfile('slide2')){
        $slide2_filename = md5(date('slide2 m/d/Y h:i:s a', time())).".".$slide2->getClientOriginalExtension();
        $slide2->move(public_path('images'), $slide2_filename);
        $event['slide2'] = $slide2_filename;
      }

      if ($request->hasfile('slide3')){
        $slide3_filename = md5(date('slide3 m/d/Y h:i:s a', time())).".".$slide3->getClientOriginalExtension();
        $slide3->move(public_path('images'), $slide3_filename);
        $event['slide3'] = $slide3_filename;
      }

      $event->save();


		return redirect()->route('event.manager', $event->slug);
	}

	// REFATORAR
	public function update(Request $request)
	{
		$event = $this->event->findBySlug($request->slug);
    $data = $request->toArray();

		$slide1 = $request->file('slide1');
    $slide2 = $request->file('slide2');
    $slide3 = $request->file('slide3');

    if ($request->hasfile('slide1')){
      $slide1_filename = md5(date('slide1 m/d/Y h:i:s a', time())).".".$slide1->getClientOriginalExtension();
      $slide1->move(public_path('images'), $slide1_filename);
      $event['slide1'] = $slide1_filename;
    }

    if ($request->hasfile('slide2')){
      $slide2_filename = md5(date('slide2 m/d/Y h:i:s a', time())).".".$slide2->getClientOriginalExtension();
      $slide2->move(public_path('images'), $slide2_filename);
      $event['slide2'] = $slide2_filename;
    }

    if ($request->hasfile('slide3')){
      $slide3_filename = md5(date('slide3 m/d/Y h:i:s a', time())).".".$slide3->getClientOriginalExtension();
      $slide3->move(public_path('images'), $slide3_filename);
      $event['slide3'] = $slide3_filename;
    }

		$slug_request = str_slug($request->title, '-');

		$slug = $event->slug == $slug_request ? $event->slug : $slug_request;

		$request['slug'] = $slug;

		$event->update($data);

		return redirect()->route('event.list');
	}

	public function manager($slug)
	{
		$event = $this->event->findBySlug($slug);
    $programs = $event->programs;
    $publications = $event->publications;

		return view('event.manager', compact('event'));
	}

	public function destroy($id)
  {
    $this->event->destroy($id);

    return redirect()->back();
  }

  public function status($id)
  {
  	$this->event->toggleStatus($id);

  	return redirect()->back();
  }

}
