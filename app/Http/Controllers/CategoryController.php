<?php

namespace App\Http\Controllers;

use App\Repositories\CategoryRepository;

use Illuminate\Http\Request;
use App\Models\Category;
//use App\Models\Course;
use Redirect;

class CategoryController extends Controller
{
  private $category;

    public function __construct(CategoryRepository $category){
        $this->category = $category;
    }

  public function list()
	{
		$categories = $this->category->all();

		return view('category.list')->with(compact('categories'));
	}

  public function create(){
  	$category = Category::all();
  	return view('category.create')->with(compact('category'));

  }

  public function store(Request $request){
    $category = $this->category->create($request->toArray());

		$category->save();

		return redirect()->route('category.list');
  }

  public function edit($slug)
  {
      $category = $this->category->findBySlug($slug);

      return view('category.edit', compact('category'));
  }

  public function update(Request $request)
  {
      $category = $this->category->findBySlug($request->slug);

      $slug_request = str_slug($request->name, '-');

			$slug = $category->slug == $slug_request ? $category->slug : $slug_request;

			$request['slug'] = $slug;



      $category->update($request->toArray());

      return redirect()->route('category.list');
  }

  public function destroy($id){
     $this->category->destroy($id);

    return redirect()->back();
  }
}
