<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\PostRepository;
use App\Models\Post;

class PostController extends Controller
{
  private $post;

  public function __construct(PostRepository $post){
      $this->post = $post;
  }

  public function index(){
    $posts = Post::orderBy('created_at', 'DESC')->paginate(10);

    return view('site.post', compact('posts'));
  }

  public function list(){
  	$posts = $this->post->all();

  	return view('news.list', compact('posts'));
  }

  public function create(){
  $posts = $this->post->all();

	return view('news.create', compact('posts'));
	}

  public function store(Request $request) {
  	$post = $this->post->create($request->toArray());

	  $photo = $request->file('photo');

	  if ($request->hasfile('photo')){
	    $photo_filename = md5(date('photo m/d/Y h:i:s a', time())).".".$photo->getClientOriginalExtension();
	    $photo->move(public_path('images'), $photo_filename);
	    $post['photo'] = $photo_filename;
	  }

	  $post->save();

  	return redirect()->route('news.list');

  }

  public function edit($slug)
  {
    $post = $this->post->findBySlug($slug);

    return view('news.edit', compact('post'));
  }

  public function update(Request $request)
  {
    $post = $this->post->findBySlug($request->slug);

	  $photo = $request->file('photo');
	  $data = $request->toArray();

	  if ($request->hasfile('photo')){
	    $photo_filename = md5(date('photo m/d/Y h:i:s a', time())).".".$photo->getClientOriginalExtension();
	    $photo->move(public_path('images'), $photo_filename);
	    $data['photo'] = $photo_filename;
	  }

    $slug_request = str_slug($request->title, '-');

		$slug = $post->slug == $slug_request ? $post->slug : $slug_request;

		$data['slug'] = $slug;

    $post->update($data);

    return redirect()->route('news.list');
  }

  public function destroy($id)
  {
    $this->post->destroy($id);

    return redirect()->back();
  }
}
