<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CourseRepository;
use App\Repositories\CategoryRepository;

use App\Models\Course;
use App\Models\Category;

class CourseController extends Controller
{
  private $course;
  private $category;

    public function __construct(CourseRepository $course, CategoryRepository $category){
        $this->course = $course;
        $this->category = $category;
    }

  public function index($category){
    $category = Category::where('slug', $category)->first();


    return view('site.course', compact('category'));
  }

  public function getCourse($id)
  {
    return Course::find($id);
  }


  public function list($slug)
	{
		$category = $this->category->findBySlug($slug);

    $courses = $category->courses;

		return view('course.list')->with(compact('courses', 'category'));
	}

  public function create($slug){
  	$courses = $this->course->all();
  	$category = $this->category->findBySlug($slug);

  	return view('course.create')->with(compact('category'));
  }

  public function store(Request $request){
  	$course = $this->course->create($request->toArray());
  	$course->save();

  	return redirect()->route('course.list', $course->category->slug);
  }

  public function edit($slug)
  {
    $course = $this->course->findBySlug($slug);

    return view('course.edit', compact('course'));
  }

  public function update(Request $request)
  {
    $course = $this->course->findBySlug($request->slug);

    $slug_request = str_slug($request->name, '-');

		$slug = $course->slug == $slug_request ? $course->slug : $slug_request;

		$request['slug'] = $slug;

    $course->update($request->toArray());

    return redirect()->route('course.list', $course->category->slug);
  }

  public function destroy($id){
	  $this->course->destroy($id);

    return redirect()->back();
  }
}
