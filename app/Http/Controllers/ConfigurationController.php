<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Configuration;

use Redirect;

class ConfigurationController extends Controller
{
  public function index(){
    $configuration = Configuration::first();

    return view('index', compact('configuration'));
  }

  public function create(){
      $configuration = Configuration::first();
      if ($configuration == null) {
        $configuration = new Configuration();

        return view('configuration.setting')->with(compact('configuration'));
   		}


    return view('configuration.setting')->with(compact('configuration'));
  }

  public function store(Request $request)
  {
    $config = Configuration::first();

    $slide1 = $request->file('slide1');
    $slide2 = $request->file('slide2');
    $slide3 = $request->file('slide3');
    $logo = $request->file('logo');
    
    $data = $request->toArray();

    if ($request->hasfile('slide1')){
      $slide1_filename = md5(date('slide1 m/d/Y h:i:s a', time())).".".$slide1->getClientOriginalExtension();
      $slide1->move(public_path('images'), $slide1_filename);
      $data['slide1'] = $slide1_filename;
    }

    if ($request->hasfile('slide2')){
      $slide2_filename = md5(date('slide2 m/d/Y h:i:s a', time())).".".$slide2->getClientOriginalExtension();
      $slide2->move(public_path('images'), $slide2_filename);
      $data['slide2'] = $slide2_filename;
    }

    if ($request->hasfile('slide3')){
      $slide3_filename = md5(date('slide3 m/d/Y h:i:s a', time())).".".$slide3->getClientOriginalExtension();
      $slide3->move(public_path('images'), $slide3_filename);
      $data['slide3'] = $slide3_filename;
    }

    if ($request->hasfile('logo')){
      $logo_filename = md5(date('logo_iegm m/d/Y h:i:s a', time())).".".$logo->getClientOriginalExtension();
      $logo->move(public_path('images'), $logo_filename);
      $data['logo'] = $logo_filename;
    }

    if ($config != null) {
      $config->update($data);
    } else {
      $config = Configuration::create($data);
    }          

	  return Redirect::back()->with('success', 'Modificações salvas com sucesso!');
  }

}
