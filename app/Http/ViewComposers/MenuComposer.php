<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
// use App\Event;
use App\Models\Category;
use App\Models\Configuration;
use App\Models\Event;
use App\Models\Course;

class MenuComposer
{
    
    protected $categories;
    protected $configurations;
    protected $events;
    protected $courses;

    public function __construct(Category $categories, Configuration $configurations, Event $events, Course $courses)
    {
        $this->categories = $categories->all();
        $this->configurations = $configurations->first();
        $this->events = $events->all();
        $this->courses = $courses->all();
    }

    public function compose(View $view)
    {
        $view->with('categories', $this->categories);
        $view->with('configurations', $this->configurations);
        $view->with('events', $this->events);
        $view->with('courses', $this->courses);
    }
}