<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
// use App\Event;
use App\Models\Configuration;

class ConfigurationComposer
{
    
    protected $configurations;
    // protected $events;

    public function __construct(Configuration $configurations)
    {
        $this->configurations = $configurations->first();
        // $this->events = $events;
    }

    public function compose(View $view)
    {;
        $view->with('configurations', $this->configurations);
        // $view->with('events', $this->events);
    }
}