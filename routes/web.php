<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/admin', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function () {

	// Eventos
	Route::get('/events', 'EventController@list')->name('event.list');
	Route::get('/event/{id}/destroy', 'EventController@destroy')->name('event.destroy');
	Route::get('/event/{id}/status', 'EventController@status')->name('event.status');
	// View (Create/Update)
	Route::get('/event/create', 'EventController@create')->name('event.create');
	Route::get('/event/{slug}/edit', 'EventController@edit')->name('event.edit');
	// Store (Create/Update)
	Route::post('/event/store', 'EventController@store')->name('event.store');
	Route::post('/event/update', 'EventController@update')->name('event.update');
	// Manager
	Route::get('/event/{slug}', 'EventController@manager')->name('event.manager');
	// Palestra
	Route::get('/event/{slug}/program/create', 'ProgramController@create')->name('program.create');
	Route::post('/event/program/store', 'ProgramController@store')->name('program.store');
	Route::get('/event/program/{id}/edit', 'ProgramController@edit')->name('program.edit');
	Route::post('/event/program/update', 'ProgramController@update')->name('program.update');
	Route::get('/event/program/{id}/destroy', 'ProgramController@destroy')->name('program.destroy');
	// Publicação
	Route::get('/event/{slug}/publication/create', 'PublicationController@create')->name('publication.create');
	Route::post('/event/publication/store', 'PublicationController@store')->name('publication.store');
	Route::get('/event/publication/{id}/edit', 'PublicationController@edit')->name('publication.edit');
	Route::post('/event/publication/update', 'PublicationController@update')->name('publication.update');
	Route::get('/event/publication/{id}/destroy', 'PublicationController@destroy')->name('publication.destroy');
	//Categoria
	Route::get('/categories', 'CategoryController@list')->name('category.list');
	Route::get('/category/create', 'CategoryController@create')->name('category.create');
	Route::post('/category/store', 'CategoryController@store')->name('category.store');
	Route::get('/category/{slug}/edit', 'CategoryController@edit')->name('category.edit');
	Route::post('/category/update', 'CategoryController@update')->name('category.update');
	Route::get('/category/{id}/destroy', 'CategoryController@destroy')->name('category.destroy');
	//Curso
	Route::get('/category/{slug}/courses', 'CourseController@list')->name('course.list');
	Route::get('/category/{slug}/course/create', 'CourseController@create')->name('course.create');
	Route::post('/category/course/store', 'CourseController@store')->name('course.store');
	Route::get('/course/{slug}/edit', 'CourseController@edit')->name('course.edit');
	Route::post('/category/course/edit', 'CourseController@update')->name('course.update');
	Route::get('/category/course/{id}/destroy', 'CourseController@destroy')->name('course.destroy');
	//Notícias
	Route::get('/news', 'PostController@list')->name('news.list');
	Route::get('/news/create', 'PostController@create')->name('news.create');
	Route::post('/news/store', 'PostController@store')->name('news.store');
	Route::get('/news/{slug}/edit', 'PostController@edit')->name('news.edit');
	Route::post('/news/update', 'PostController@update')->name('news.update');
	Route::get('/news/{id}/destroy', 'PostController@destroy')->name('news.destroy');
	//Configuração
	Route::get('/setting', 'ConfigurationController@create')->name('configuration.create');
	Route::post('/setting', 'ConfigurationController@store')->name('configuration.store');

});

//Site
Route::get('/', 'ConfigurationController@index')->name('index');
Route::get('/instituicao', 'IEGMController@index')->name('institution');
Route::get('/cursos/{slug}', 'CourseController@index')->name('category.courses');
Route::get('/curso/{id}', 'CourseController@getCourse')->name('course.show');
Route::get('/contato', 'ContactController@index')->name('contact');
Route::get('/imprensa', 'PostController@index')->name('post');
Route::get('/event/{slug}', 'EventController@index')->name('event');
